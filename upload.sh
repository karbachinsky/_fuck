#!/usr/bin/env bash

cd $1

rm -rf *.zip

NAME=aiay`date +"%H%M"`.zip
echo $NAME > file.file

zip -r $NAME *

curl \
  -F "file=@$NAME" \
  alfahack.dhis.org/upload