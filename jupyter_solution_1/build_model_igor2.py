import numpy as np
import sys
import lightgbm as lgb
import time
from sklearn.metrics import mean_squared_error
from datetime import datetime, timedelta

import multiprocessing as mp

from collections import deque


def main():
    print('Collecting dataset')

    dataset, y = collect_dataset(sys.argv[1])

    print('Learn')

    learn(dataset, y, sys.argv[2] if len(sys.argv) >=3 else str(time.time()))


columns = [
        'ID', 'TS',
        'BID0', 'VBID0', 'BID1', 'VBID1', 'BID2', 'VBID2', 'BID3', 'VBID3', 'BID4', 'VBID4',
        'BID5', 'VBID5', 'BID6', 'VBID6', 'BID7', 'VBID7', 'BID8', 'VBID8', 'BID9', 'VBID9',
        'ASK0', 'VASK0', 'ASK1', 'VASK1', 'ASK2', 'VASK2', 'ASK3', 'VASK3', 'ASK4', 'VASK4',
        'ASK5', 'VASK5', 'ASK6', 'VASK6', 'ASK7', 'VASK7', 'ASK8', 'VASK8', 'ASK9', 'VASK9', 'Y'
]
exclude_columns = set(['Y', 'ID', 'TS'])
not_number_columns = set(['ID', 'TS'])

t0 = time.time()


def collect_dataset(dataset_path):
    f = open(dataset_path, 'r')

    dataset = []
    target = []

    f.readline()

    collector = deque()

    keys = None

    for line in f:
        row = dict(
            list(
                map(
                    lambda k_v: (k_v[0], float(k_v[1])) if k_v[0] not in not_number_columns else k_v,
                    zip(columns, line.split(';'))
                )
            )
        )

        if row['ID'] != 'TEA':
            continue

        row = get_features(row, collector)

        if keys is None:
            keys = sorted(row.keys())

        dataset.append(
            np.array([row[k] for k in keys if k not in exclude_columns])
        )

        target.append(row['Y'])

        len_dataset = len(dataset)

        if len_dataset % 1000 == 0:
            print(len_dataset)
            global t0
            print(' took 2 %0.3f ms' % ((time.time() - t0) * 1000.0))
            t0 = time.time()

        collector.append(row)

        if len(collector) > 1000:
            collector.popleft()

    return dataset, target


def get_features(row, collector):
    #row['AVG_BID'] = np.mean([row[k] for k in row.keys() if k.startswith('BID')])
    #row['AVG_ASK'] = np.mean([row[k] for k in row.keys() if k.startswith('ASK')])
    #row['AVG_VASK'] = np.mean([row[k] for k in row.keys() if k.startswith('VASK')])
    #row['AVG_VBID'] = np.mean([row[k] for k in row.keys() if k.startswith('VBID')])

    super_features = calculate_metrics(collector)

    row.update(super_features)

    return row


def rmse(ytrue, ypred):
    return np.sqrt(mean_squared_error(ytrue, ypred))


def learn(dataset, y, model_suffix_name=str(time.time())):
    assert(0 == np.count_nonzero(np.isnan(dataset)))
    assert(0 == np.count_nonzero(np.isnan(y)))

    sep = int(0.65 * len(dataset))
    lgbtrain = lgb.Dataset(dataset[:sep], label=y[:sep])
    lgbtest = lgb.Dataset(dataset[sep:], label=y[sep:])

    lgbparam = {}
    lgbparam['metric'] = 'rmse'
    lgbparam['application'] = 'regression'
    lgbparam['nthread'] = 4
    lgbparam['seed'] = 1
    lgbnumround = 200

    model = lgb.train(lgbparam, lgbtrain, lgbnumround, valid_sets=[lgbtrain, lgbtest], verbose_eval=10)
    ypred = model.predict(dataset)

    if sep != 0:
        score1 = rmse(y[:sep], ypred[:sep])
        score2 = rmse(y[sep:], ypred[sep:])

        print(score1)
        print(score2)

    model_filename = 'my_model_{}.txt'.format(model_suffix_name)
    model.save_model(model_filename)

    print('Saved model: {}'.format(model_filename))


def process_var(q_in, q_out):
    while True:
        key, task, data = q_in.get()
        if task == 'var':
            out = np.var(data)
        elif task == 'percentile':
            out = np.percentile(data, 90)
        q_out.put((key, task, out))

#var_queue_in = mp.Queue()
#var_queue_out = mp.Queue()
# mp.set_start_method('spawn')
#for i in range(4):
#    var_process = mp.Process(target=process_var, args=(var_queue_in, var_queue_out))
#    var_process.start()


def calculate_metrics(dataset):
    pre_last_index = len(dataset) - 2 if len(dataset) > 1 else 0

    if len(dataset) == 0:
        dataset = [{
            k: 0. for k in columns
        }]

    result = {}
    # print(dataset[0])

    #time1 = time.time()
    bid0 = list(map(lambda x: x['BID3'], dataset))
    ask0 = list(map(lambda x: x['ASK3'], dataset))

    # bid0 = list(map(lambda x: sum([x['BID%s' % i] for i in range(10)]) / 10.0, dataset))
    # ask0 = list(map(lambda x: sum([x['ASK%s' % i] for i in range(10)]) / 10.0, dataset))

    #time2 = time.time()

    result['max_bid0'] = max(bid0)
    result['min_bid0'] = min(bid0)
    #   var_queue_in.put(('var_bid0', 'var', bid0))
    result['var_bid0'] = np.var(bid0)
    result['max_ask0'] = max(ask0)
    result['min_ask0'] = min(ask0)
    #var_queue_in.put(('var_ask0', 'var', ask0))

    result['var_ask0'] = np.var(ask0)

    result['percentile_ask0'] = np.percentile(ask0, 90)
    result['percentile_bid0'] = np.percentile(bid0, 90)
    #var_queue_in.put(('percentile_ask0', 'percentile', ask0))
    #var_queue_in.put(('percentile_bid0', 'percentile', bid0))

    for _ in range(4):
        key, task, data = var_queue_out.get()
        result[key] = data

    result['delta_max_bid0'] = result['max_bid0'] - dataset[pre_last_index].get('max_bid0', 0)
    result['delta_min_bid0'] = result['min_bid0'] - dataset[pre_last_index].get('min_bid0', 0)
    result['delta_var_bid0'] = result['var_bid0'] - dataset[pre_last_index].get('var_bid0', 0)
    result['delta_max_ask0'] = result['max_ask0'] - dataset[pre_last_index].get('max_ask0', 0)
    result['delta_min_ask0'] = result['min_ask0'] - dataset[pre_last_index].get('min_ask0', 0)
    result['delta_var_ask0'] = result['var_ask0'] - dataset[pre_last_index].get('var_ask0', 0)

    result['delta_percentile_bid0'] = result['percentile_bid0'] - dataset[pre_last_index].get('percentile_bid0', 0)
    result['delta_percentile_ask0'] = result['percentile_ask0'] - dataset[pre_last_index].get('percentile_ask0', 0)

    #time3 = time.time()

    # print(' took 1 %0.3f ms' % ((time2 - time1) * 1000.0))
    # print(' took 2 %0.3f ms' % ((time3 - time2) * 1000.0))

    # print(result)
    return result


if __name__ == '__main__':
    main()
