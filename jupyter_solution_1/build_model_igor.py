import numpy as np
import sys
import lightgbm as lgb
import time
from sklearn.metrics import mean_squared_error
from datetime import datetime, timedelta

from math import log
import multiprocessing as mp

from collections import deque


def main():
    print('Collecting dataset')

    dataset, y = collect_dataset(sys.argv[1])

    print('Learn')

    learn(dataset, y, sys.argv[2] if len(sys.argv) >=3 else str(time.time()))


columns = [
        'ID', 'TS',
        'BID0', 'VBID0', 'BID1', 'VBID1', 'BID2', 'VBID2', 'BID3', 'VBID3', 'BID4', 'VBID4',
        'BID5', 'VBID5', 'BID6', 'VBID6', 'BID7', 'VBID7', 'BID8', 'VBID8', 'BID9', 'VBID9',
        'ASK0', 'VASK0', 'ASK1', 'VASK1', 'ASK2', 'VASK2', 'ASK3', 'VASK3', 'ASK4', 'VASK4',
        'ASK5', 'VASK5', 'ASK6', 'VASK6', 'ASK7', 'VASK7', 'ASK8', 'VASK8', 'ASK9', 'VASK9', 'Y'
]
exclude_columns = set(['Y', 'ID', 'TS'])
not_number_columns = set(['ID', 'TS'])

t0 = time.time()

LAG = 250


def collect_dataset(dataset_path):
    f = open(dataset_path, 'r')

    dataset = []
    target = []

    f.readline()

    collector = deque()
    coffee_collector = deque()

    keys = None

    for line in f:
        row = dict(
            list(
                map(
                    lambda k_v: (k_v[0], float(k_v[1])) if k_v[0] not in not_number_columns else k_v,
                    zip(columns, line.split(';'))
                )
            )
        )

        if row['ID'] != 'TEA':
            coffee_collector.append(row)
            if len(coffee_collector) > LAG:
                coffee_collector.popleft()

            continue

        target.append(row['Y'])

        row = get_features(row, collector, coffee_collector)

        #print(row)
        if keys is None:
            keys = sorted(row.keys())

        #import pprint
        #pprint.pprint(row)
        z = np.array([row[k] for k in keys if k not in exclude_columns])

        dataset.append(z)

        len_dataset = len(dataset)

        if len_dataset % 1000 == 0:
            print(len_dataset)
            global t0
            print(' took 2 %0.3f ms' % ((time.time() - t0) * 1000.0))
            t0 = time.time()

        collector.append(row)
        if len(collector) > LAG:
            collector.popleft()

    return dataset, target


def get_features(row, collector, coffee_collector=None):
    r = {
        'AVG_BID': np.mean([row[k] for k in row.keys() if k.startswith('BID')]),
        'AVG_ASK': np.mean([row[k] for k in row.keys() if k.startswith('ASK')]),
        'AVG_VBID': np.mean([row[k] for k in row.keys() if k.startswith('VBID')]),
        'AVG_VASK': np.mean([row[k] for k in row.keys() if k.startswith('VASK')]),
        'BID2': row['BID2'],
        'ASK2': row['ASK2'],
        'VBID2': row['VBID2'],
        'VASK2': row['VASK2'],
        'BID0': row['BID0'],
        'ASK0': row['ASK0'],
        'BID1': row['BID1'],
        'ASK1': row['ASK1'],
        'lBID2': log(row['BID2']),
        'lASK2': log(row['ASK2']),
    }

    if collector:
        _index = 0
        r['BID2_lg'] = log(row['BID2']) - collector[_index]['BID2_lg']
        r['ASK2_lg'] = log(row['ASK2']) - collector[_index]['ASK2_lg']
        r['BID0_lg'] = log(row['BID0']) - collector[_index]['BID0_lg']
        r['ASK0_lg'] = log(row['ASK0']) - collector[_index]['ASK0_lg']
        r['BID1_lg'] = log(row['BID1']) - collector[_index]['BID1_lg']
        r['ASK1_lg'] = log(row['ASK1']) - collector[_index]['ASK1_lg']
    else:
        r['BID2_lg'] = 0.
        r['ASK2_lg'] = 0.
        r['BID0_lg'] = 0.
        r['ASK0_lg'] = 0.
        r['BID1_lg'] = 0.
        r['ASK1_lg'] = 0.

    row['AVG_VASK'] = np.mean([row[k] for k in row.keys() if k.startswith('VASK')])
    row['AVG_VBID'] = np.mean([row[k] for k in row.keys() if k.startswith('VBID')])

    super_features = calculate_metrics(collector)

    if coffee_collector is not None:
        coffee_features = calculate_metrics(coffee_collector, full=True)

        r.update({
            'coffee_{}'.format(k):v for k,v in coffee_features.items()
        })


    r.update(super_features)

    return r


def rmse(ytrue, ypred):
    return np.sqrt(mean_squared_error(ytrue, ypred))


def learn(dataset, y, model_suffix_name=str(time.time())):
    assert(0 == np.count_nonzero(np.isnan(dataset)))
    assert(0 == np.count_nonzero(np.isnan(y)))

    sep = int(0.6 * len(dataset))
    lgbtrain = lgb.Dataset(dataset[:sep], label=y[:sep])
    lgbtest = lgb.Dataset(dataset[sep:], label=y[sep:])

    lgbparam = {}
    lgbparam['metric'] = 'rmse'
    lgbparam['application'] = 'regression'
    lgbparam['nthread'] = 5
    lgbparam['seed'] = 2
    lgbparam['max_depth'] = 2
    lgbparam['learning_rate'] = 0.011

    lgbparam.update({
        #'feature_fraction': 0.9,
        'bagging_fraction': 0.9,
        'bagging_freq': 2,
        'lambda_l2': 0.1,
        'lambda_l1': 0.1,
    })


    #lgbparam['bagging_fraction'] = 0.5
    #lgbparam['bagging_freq'] = 2
    #lgbparam['feature_fraction'] = 0.9810382151125093
    #lgbparam['lambda_l1'] = 2.964179981087924
    #lgbparam['lambda_l2'] = 0.9044175522243257
    #lgbparam['max_bin'] = 275
    #lgbparam['min_data_in_leaf'] = 146.0
    #lgbparam['min_sum_hessian_in_leaf'] = 1.4759301328359902

    lgbnumround = 200

    model = lgb.train(lgbparam, lgbtrain, lgbnumround, valid_sets=[lgbtrain, lgbtest], verbose_eval=10)
    ypred = model.predict(dataset)

    if sep != 0:
        score1 = rmse(y[:sep], ypred[:sep])
        score2 = rmse(y[sep:], ypred[sep:])

        print(score1)
        print(score2)

    model_filename = 'my_model_{}.txt'.format(model_suffix_name)
    model.save_model(model_filename)

    print('Saved model: {}'.format(model_filename))


def process_var(q_in, q_out):
    while True:
        key, task, data = q_in.get()
        if key is None:
            return
        if task == 'var':
            out = np.var(data)
        elif task == 'percentile':
            out = np.percentile(data, 90)
        elif task == 'mean':
            out = np.mean(data)
        q_out.put((key, task, out))

#var_queue_in = mp.Queue()
#var_queue_out = mp.Queue()
# mp.set_start_method('spawn')

#for i in range(4):
#    var_process = mp.Process(target=process_var, args=(var_queue_in, var_queue_out))
#    var_process.start()


def calculate_metrics(dataset, full=True):
    if len(dataset) == 0:
        dataset = [{
            k: 0. for k in columns
        }]

    #time1 = time.time()
    bid0 = list(map(lambda x: x['BID2'], dataset))
    ask0 = list(map(lambda x: x['ASK2'], dataset))

    # bid0 = list(map(lambda x: sum([x['BID%s' % i] for i in range(10)]) / 10.0, dataset))
    # ask0 = list(map(lambda x: sum([x['ASK%s' % i] for i in range(10)]) / 10.0, dataset))

    #time2 = time.time()

    max_bid0 = max(bid0)
    min_bid0 = min(bid0)
    #   var_queue_in.put(('var_bid0', 'var', bid0))
    var_bid0 = np.var(bid0)
    max_ask0 = max(ask0)
    min_ask0 = min(ask0)
    #var_queue_in.put(('var_ask0', 'var', ask0))

    var_ask0 = np.var(ask0)

    percentile_ask0 = np.percentile(ask0, 50)
    percentile_bid0 = np.percentile(bid0, 50)
    #var_queue_in.put(('percentile_ask0', 'percentile', ask0))
    #var_queue_in.put(('percentile_bid0', 'percentile', bid0))

    #for _ in range(4):
    #    key, task, data = var_queue_out.get()
    #    result[key] = data

    prev_row = dataset[0] # -1

    w = np.exp(np.array([i for i in range(len(ask0))]))

    #assert False, np.diff(bid0)
    result = {
        'percentile_ask0': percentile_ask0,
        'percentile_bid0': percentile_bid0,
        'max_bid0': max_bid0,
        'min_bid0': min_bid0,
        #'var_bid0': var_bid0,
        'max_ask0': max_ask0,
        'min_ask0': min_ask0,
        #'var_ask0': var_ask0,

        'std_ask0': np.std(ask0),
        'std_bid0': np.std(bid0),

        'std_ask0_log': np.std(np.log(1 + np.array(ask0))),
        'std_bid0_log': np.std(np.log(1 + np.array(bid0))),

        'diff_bid0': max_bid0 - min_bid0,
        'diff_ask0': max_ask0 - min_ask0,

        'exp_mean_ask0': np.average(ask0, weights=w),
        'exp_mean_bid0': np.average(bid0, weights=w),

        'TR_bid0': max(max_bid0 - min_bid0, max_bid0 - prev_row['BID2'], prev_row['BID2'] - min_bid0),
        'TR_ask0': max(max_bid0 - min_bid0, max_bid0 - prev_row['ASK2'], prev_row['ASK2'] - min_bid0),

        'ATR_bid0': np.mean(list(map(lambda x: x.get('TR_bid0', 0.), dataset))),
        'ATR_ask0': np.mean(list(map(lambda x: x.get('TR_ask0', 0.), dataset))),
    }
    if full:
        result.update({
            'xxx_ask0': xxx(ask0),
            'xxx_bid0': xxx(bid0),
            #'mean_abs_change_bid0': np.mean(np.abs(np.diff(bid0))) if len(bid0) > 1 else 0.,
            #'mean_abs_change_ask0': np.mean(np.abs(np.diff(ask0))) if len(bid0) > 1 else 0.,
            #'delta_max_bid0': max_bid0 - dataset[-1].get('max_bid0', 0),
            #'delta_min_bid0': min_bid0 - dataset[-1].get('min_bid0', 0),
            #'delta_var_bid0': var_bid0 - dataset[-1].get('var_bid0', 0),
            #'delta_max_ask0': max_ask0 - dataset[-1].get('max_ask0', 0),
            #'delta_min_ask0': min_ask0 - dataset[-1].get('min_ask0', 0),
            #'delta_var_ask0': var_ask0 - dataset[-1].get('var_ask0', 0),

            #'delta_percentile_bid0': percentile_bid0 - dataset[-1].get('percentile_bid0', 0),
            #'delta_percentile_ask0': percentile_ask0 - dataset[-1].get('percentile_ask0', 0)
        })


    #time3 = time.time()

    # print(' took 1 %0.3f ms' % ((time2 - time1) * 1000.0))
    # print(' took 2 %0.3f ms' % ((time3 - time2) * 1000.0))

    # print(result)
    return result


def xxx(ask0):
    n = len(ask0)
    x = np.asarray(ask0)
    if 2 * LAG >= n:
        return 0.
    return np.mean(
        (np.roll(x, 2 * -LAG) * np.roll(x, 2 * -LAG) * np.roll(x, -LAG)-np.roll(x, -LAG) * x * x)[0:n - 2 * LAG]
    )

if __name__ == '__main__':
    processes = []
    PROCESS_NUM = 4
    #for i in range(PROCESS_NUM):
    #    var_process = mp.Process(target=process_var, args=(var_queue_in, var_queue_out))
    #    var_process.start()
    #    processes.append(var_process)

    main()

    #for _ in range(PROCESS_NUM):
    #    var_queue_in.put((None, None, None))

    #for process in processes:
    #    process.join()


