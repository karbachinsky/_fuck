#!/usr/bin/python

from __future__ import print_function # for python 2 compatibility

import math

import hackathon_protocol
import os
import lightgbm as lgb
import pandas as pd
from build_model_igor import *
import numpy as np
from collections import deque
import datetime

USERNAME="AIAY"
PASSWORD="5fotzh0z316xj6hk"

CONNECT_IP = os.environ.get("HACKATHON_CONNECT_IP") or "127.0.0.1"
CONNECT_PORT = int(os.environ.get("HACKATHON_CONNECT_PORT") or 12345)


WINDOW_SIZE = 100


class MyClient(hackathon_protocol.Client):

    rows_to_store = 100
    _reallocate_threshold = rows_to_store + 10000

    #MODEL_NAME = 'my_model_1512279662.868045.txt'
    MODEL_NAME = 'my_model_1512288705.3403707.txt'

    #MODEL_NAME = 'my_model_pobeda.txt'

    def __init__(self, sock):
        super(MyClient, self).__init__(sock)
        self.counter = 0
        self.target_instrument = 'TEA'
        self.send_login(USERNAME, PASSWORD)
        self.last_raw = None

        # Load pre-trained model previously created by create_model.ipynb
        self.model = lgb.Booster(model_file=self.MODEL_NAME)

        # store rows for train
        self.tea_rows = deque()
        self.coffee_rows = deque()
        self.tea_mid_prices = []
        self.coffee_mid_prices = []

    def on_header(self, csv_header):
        self.header = {column_name: n for n, column_name in enumerate(csv_header)}
        #columns = ['ID', 'TS',
        #          'BID0', 'VBID0', 'BID1', 'VBID1', 'BID2', 'VBID2', 'BID3', 'VBID3', 'BID4', 'VBID4',
        #          'BID5', 'VBID5', 'BID6', 'VBID6', 'BID7', 'VBID7', 'BID8', 'VBID8', 'BID9', 'VBID9',
        #          'ASK0', 'VASK0', 'ASK1', 'VASK1', 'ASK2', 'VASK2', 'ASK3', 'VASK3', 'ASK4', 'VASK4',
        #          'ASK5', 'VASK5', 'ASK6', 'VASK6', 'ASK7', 'VASK7', 'ASK8', 'VASK8', 'ASK9', 'VASK9', 'Y']
        # store last dataset
        # self.dataset = pd.DataFrame(columns=columns)
        # self.columns = columns
        # print("Dataset:", self.dataset)

    keys = None

    is_updated = None
    is_coffee = None

    def on_orderbook(self, row):
        self.is_updated = False

        row = dict(
            list(
                map(
                    lambda k_v: (k_v[0], float(k_v[1])) if k_v[0] not in not_number_columns else k_v,
                    zip(columns, row)
                )
            )
        )

        if row['ID'] != 'TEA':
            self.is_coffee = True
            return

        was_coffee = self.is_coffee
        self.is_coffee = False

        if self.t % 5 != 0 and not was_coffee:
            return

        self.is_updated = True

        self.last_raw = get_features(row, self.tea_rows, self.coffee_rows)

        if self.keys is None:
            self.keys = sorted(self.last_raw)

        row.update(self.last_raw)
        self.last_raw = np.array([self.last_raw[k] for k in self.keys if k not in exclude_columns])

        # for save rows
        self._append_row(row)

    t = 0
    prediction = None

    def make_prediction(self):
        if self.is_coffee:
            p = 0.
        elif self.is_updated:
            assert self.last_raw is not None
            p = self.model.predict([self.last_raw])[0]
            self.prediction = p
        else:
            p = self.prediction

        self.t += 1
        answer = p

        #print(answer, self.t, self.is_coffee)

        self.send_volatility(answer)
        self.last_raw = None

    def on_score(self, items_processed, time_elapsed, score_value):
        print("Completed! items processed: %d, time elapsed: %.3f sec, score: %.6f" % (items_processed, time_elapsed, score_value))
        self.stop()

    def train(self, row):
        """
        :param row: csv_line_values + Y
        """
        # print('train', row)
        # TODO train here
        pass

    def _append_row(self, csv_line_values):
        # append to dataset - FIXME very slow
        # self.dataset.loc[-1] = csv_line_values + [None, ] # adding a row
        # self.dataset.index = self.dataset.index + 1  # shifting index
        self._append_for_train(csv_line_values)

    _last_added_ts = None

    def _append_for_train(self, row):
        # append for train and cal Y

        if self._last_added_ts is not None:
            ts = datetime.strptime(row['TS'], '%Y.%m.%d %H:%M:%S.%f')
            if ts.seconds == self._last_added_ts.seconds:
                return

            self._last_added_row = ts

        instrument = row['ID']
        if instrument == 'TEA':
            rows = self.tea_rows
            mid_prices = self.tea_mid_prices
        else:
            rows = self.coffee_rows
            mid_prices = self.coffee_mid_prices

        rows.append(row)

        if len(rows) > LAG:
            rows.popleft()

        #assert False, row

        #best_bid = int(row['BID0'])
        #best_ask = int(row['ASK0'])

        #mid_prices.append((best_bid + best_ask) / 2.0)
        #if len(mid_prices) >= WINDOW_SIZE:
        #    y100 = self._last_volatility(mid_prices)
        #    rows[-WINDOW_SIZE]['Y'] = y100
        #    self.train(rows[-WINDOW_SIZE])

    @staticmethod
    def _last_volatility(mid_prices):
        assert WINDOW_SIZE > 1

        if len(mid_prices) < WINDOW_SIZE:
            return 0

        window = mid_prices[-WINDOW_SIZE:]
        mean = sum(window) / WINDOW_SIZE
        return math.sqrt(sum([(x - mean) ** 2 for x in window]) / (WINDOW_SIZE - 1))


def on_connected(sock):
    client = MyClient(sock)
    client.run()


def main():
    hackathon_protocol.tcp_connect(CONNECT_IP, CONNECT_PORT, on_connected)


if __name__ == '__main__':
    main()
